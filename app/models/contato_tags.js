'use strict'

module.exports = (sequelize, DataTypes) => {

var Contato_Tags = sequelize.define('contato_tag', 
{
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },

    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },

    TagId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },

    ContatoId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },     


  //   contatoid : {
  //     type: DataTypes.INTEGER,
  //     references: {
  //       model: 'contatos', // name of Target model
  //       key: 'id', // key in Target model that we're referencing
  //     },
  //     // onUpdate: 'CASCADE',
  //     // onDelete: 'SET NULL',
  //  },

  //  tagid : {
  //     type: DataTypes.INTEGER,
  //     references: {
  //       model: 'tags', // name of Target model
  //       key: 'id', // key in Target model that we're referencing
  //     },
      // onUpdate: 'CASCADE',
      // onDelete: 'SET NULL',
 // },
}

)

return Contato_Tags;

}