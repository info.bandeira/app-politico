
module.exports = (app, model)=>{

    const Op = model.Sequelize.Op; 

    app.get('/maps', function(req, res){

        res.render('maps/contato')
    })
  
     
    app.get('/maps/index', function(req, res){

        res.render('maps/index')
    })

    app.get('/maps/json', function(req, res){
        
       model.contato.findAll({
           where : {
               latitude : {[Op.not] :  null},
               longitude : {[Op.not] :  null},
           }
       }).then(contatos => {
      
        var list = [];

        contatos.forEach(element => {
            // coords:{lat:-8.762996,lng: -63.835537},
               var objeto = {
                  coords : {lat : Number(element.latitude), lng : Number(element.longitude)} , 
                  content : element.nome,
               }

               list.push(objeto)
          });
             
          res.json(list);
       }) 

        // var list = [
        //     {latitude : 1, longitude : 2, content : "glaudes"},
        //     {latitude :  3, longitude : 4},
        // ];

       
    })

}