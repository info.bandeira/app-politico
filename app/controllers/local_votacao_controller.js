local_votacoes = require('../../json-local-localidade/local-votacao.json')
require('devbox-linq')


module.exports = (app, model)=>{

    app.get('/local_votacao/get_locais', (req, res)=>{
        var list = [];
 
    var text = removerAcentos(req.query.query);    

    var localidades = local_votacoes.data
        .filter(x=> x.municipio !== undefined && x.local !== undefined && x.endereco !== undefined && x.bairro !== undefined)
        .filter(x=> removerAcentos(x.municipio).includes(text) || x.zona.trim() == text.trim() 
                    || removerAcentos(x.local).includes(text) || removerAcentos(x.endereco).includes(text)
                    || removerAcentos(x.bairro).includes(text)    );
  
        localidades.forEach(elemento => {
            var objeto = {
             
                value : elemento.codigo,
                text :  'ZONA: '  + elemento.zona  + ', LOCAL: ' + elemento.local + ', ENDER: ' + elemento.endereco + ', BAIRRO: ' + elemento.bairro + ', MUNICÍPIO: ' + elemento.municipio,
            }

            list.push(objeto);
        });
        
        res.json(list);
    });

    var map={"â":"a","Â":"A","à":"a","À":"A","á":"a","Á":"A","ã":"a","Ã":"A","ê":"e","Ê":"E","è":"e","È":"E","é":"e","É":"E","î":"i","Î":"I","ì":"i","Ì":"I","í":"i","Í":"I","õ":"o","Õ":"O","ô":"o","Ô":"O","ò":"o","Ò":"O","ó":"o","Ó":"O","ü":"u","Ü":"U","û":"u","Û":"U","ú":"u","Ú":"U","ù":"u","Ù":"U","ç":"c","Ç":"C"};

function removerAcentos(s){

           return s.toUpperCase().replace(/[\W\[\] ]/g,
            function(a){
                return map[a]||a}
            ) 
};

}

    
