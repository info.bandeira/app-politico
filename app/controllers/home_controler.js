var autenticacao = require('../../middleware/autenticador');

module.exports = (app, model) =>{
    
    app.get('/home/index', autenticacao, (req, res) =>{

        res.render('home/index');
    })

    app.get('/', autenticacao, (req, res) =>{

        res.render('home/index');
    })
}

