var autenticador = require('../../middleware/autenticador');

module.exports = (app, model)=>{

    app.get('/agenda/index', autenticador, (req, res)=>{

        res.render('agenda/index')
    });

}