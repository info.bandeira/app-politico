var autenticador = require('../../middleware/autenticador');
require('devbox-linq')
var validacao = require('../../validacoes/contato')
local_votacao = require('../../json-local-localidade/local-votacao.json');

module.exports = function(app, model) {
  
    const Op = model.Sequelize.Op; 
    
    app.get('/contatos', autenticador, function(req, res){
       
        model.contato.findAll({
            raw: true,
            attributes : ['id', 'nome', 'celular1', 'tipo_de_contato'],
            where: {
                status: 1,
              } 
           }).then(list=> {
               res.render('contato/index', {list: list})
           });
    });

    app.get('/contato/create', autenticador, function(req, res){
        var body  = {
            tags : [],
        }
    
        res.render('contato/create', { body : body})
    });
   
    app.get('/contato/get_tag', autenticador, (req, res) =>
    {
       var query = req.query.query;
   
       model.sequelize.query("SELECT `tags`.id, CONCAT(`tags` .descricao, ' (', grupo_tags.descricao, ')') as descricao FROM `tags` inner join `grupo_tags` on `tags` .grupo_tag_id = `grupo_tags`.id where `tags`.`descricao` LIKE '%"+ query +"%' and `tags`.status = 1", { type: model.sequelize.QueryTypes.SELECT  })
                    .then(tags => {
                           var list = [];
                           tags.forEach(element=>{
                             obj = {};
                              obj = {
                                  value : element.id,
                                  text : element.descricao,
                              }
                              list.push(obj);
                          })
                          res.json(list);
                    });
    })

    app.get('/contato/get_contatos', function(req, res){
        
        model.contato.findAll({
            where : {
                nome  : {
                    [Op.like] : '%' + req.query.query + '%',
                }, 
                status : 1
            }
        }).then(contatos=> {

            var list = [];
            contatos.forEach(element=>{
              obj = {};
               obj = {
                   value : element.id,
                   text : element.nome,
               }
               list.push(obj);
           })

           res.json(list);
        });
    });

    app.post('/contato/create', autenticador, function(req, res){
    
    try{
       
        req.body.email1 = req.body.email1 === '' ? 'a@a.com' : req.body.email1;

        if(validacao(req,res)){

        req.body.email =  req.body.email1 ===  'a@a.com' ?  '' : req.body.email1;
        var tags = req.body.tags != undefined ? req.body.tags : [];
        tags = tags.length == 1 ? [tags] : tags;
        req.body.tipo_de_contato = req.body.tipo_contato != undefined ? [req.body.tipo_contato].join() : '';
        
        tags.forEach(element => {
            console.log(element)
        });

         model.contato.create(req.body).then(contato=> {
                 return contato;

            }).then(contato=> {
                
                tags.forEach(element => {
                    model.contato_tags.create({
                        ContatoId : contato.id,
                        TagId : element,
                            })
                     })

                model.tag.findAll({
                    raw: true,
                   // all: true, 
                   // attributes : ['id', 'descricao'],
                     where: {
                        id: {
                            [Op.in]: tags
                          }
                       },
                       
                    }).then(tags => {

                    req.flash('info', 'Registro cadastrado com sucesso');
                    res.redirect('/contatos')
                });  
    
            });
        }else{
           
            req.body.tags = [];
            res.render('contato/create', {body: req.body} )   
        }
         
    }catch(error){
       req.flash('erro', 'Erro ao cadastrar: '+ error);
       req.body.tags = [];
       res.status(500).render('contato/create', {body: req.body} )
    }    
    });

    app.get('/contato/edit', autenticador, function(req, res){
       

        model.contato.findAll({
           // raw: true,
         
            include: [
                {
                   model: model.tag, 
                   through : {attributes : ['tags.id', 'tags.descricao', 'tags.grupo_tag_id']}, //'contato_tags'
                   
                  //  nested: true 
                }],
            where: {
                id: req.query.id,
              },
              
           }).then(contato => {
            
               var json = JSON.stringify(contato)
               var body =   JSON.parse(json);
              
               //if()
               model.contato.find({
                   where:{
                       id : body[0].indicacao,
                   }
               }).then(indica =>{
                   //console.log(indica)
                   body[0].nome_indicacao = indica != null ? indica.nome : '';
                   console.log(body[0].nome_indicacao)
                   console.log("=======================")
                  res.render('contato/edit', {body: body[0]})
               })       
           });
    });

    app.post('/contato/edit', autenticador, function(req, res){
       
        try{
        
        console.log(req.body.indicacao)    
        console.log('==============================')    

        var tags = req.body.tags != undefined ? req.body.tags : [];
        tags = tags.length == 1 ? [tags] : tags;
        req.body.tipo_de_contato = req.body.tipo_contato != undefined ? [req.body.tipo_contato].join() : '';    
       
        model.contato_tags.destroy({
            where:{
             ContatoId : req.body.id,
            }
        })
        
        tags.forEach(element => {
            model.contato_tags.create({
                ContatoId : req.body.id,
                TagId : element,
                    })
             })

        model.contato.update(req.body, {where : {id: req.body.id}}).then(contato=> {
        
                model.tag.findAll({
                    //raw: true,
                   // all: true, 
                   // attributes : ['id', 'descricao'],
                     where: {
                        id: {
                            [Op.in]: tags
                          }
                       },
                       
                    }).then(tags => {
            
                    req.flash('info', 'Registro cadastrado com sucesso');
                    res.redirect('/contatos')
                  
                });
            });

    }catch(error){

        req.flash('erro', 'Erro ao cadastrar: '+ error);
        res.status(500).render('contato/edit', {body: req.body} )
    }
    });

    app.get('/contato/count_contatos', autenticador, function(req, res){

        model.contato.count({
            where:{
                status : 1,
            }
        }).then(count=>{

            res.json(count);
        });
    });

    app.get('/contato/desabilitar', autenticador, function(req, res){
    
        try{

            model.contato.update({
                status : 0,
            }, {where : {id: req.query.id}}).then(contato=>{
                
                req.flash('info', 'Registro desabilitado com sucesso');
                res.redirect('/contatos')
            });

        }catch(error){

            req.flash('error', 'Algo aconteceu com o cadastro '+ error);
            res.redirect('/contatos')
        }
    });
   
    async function get_latitude_longitude(body){

        

        return body;
    }

}


